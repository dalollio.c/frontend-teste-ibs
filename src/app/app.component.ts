import { Component, OnInit, Input} from '@angular/core';
import { FormBuilder, FormGroup } from '../../node_modules/@angular/forms';
import { CrudControlerService } from "./crud-controler.service";
import { FormValidatorService } from "./form-validator.service";
import { delay } from "q";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})

export class AppComponent implements OnInit{
  peopleForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private crudCon:CrudControlerService,
    private validIn:FormValidatorService){};

  dbS  = [[],[]];

  // ngOnInit() ARE CALLED ON LOAD PAGE
  ngOnInit(): void {
    this.createForm();
    this.setTableValues(0);
    this.setTableValues(1);
  }

  // INSERT DATA ON DB
  @Input()
  insertData(btnValue){
    // USED ASYNC FOR GIVEN A TIME TO WAIT RESPONSE
    ( async () => {
      if (this.peopleForm.value.id ==null){
        // CALL insertPeople() WHEN PEOPLE DON'T HAVE ID // NEW PEOOPLE
        this.crudCon.insertPeople(btnValue-1,this.peopleForm.value);
      } else {
        // MAKE A MERGE WHEN ID EXISTS
        this.crudCon.editPeople(btnValue-1,this.peopleForm.value);
      }
      await delay(500);
      this.dbS[btnValue-1] = this.crudCon.dbS[btnValue-1];
      this.createForm();
    })();
  }

  // TABLE UPDATE USING dbS EXISTING ON CRUD CONTROLLER SERVICE
  setTableValues(dbRef){
    this.crudCon.getAllPeople(dbRef);
    this.dbS[dbRef] = this.crudCon.dbS[dbRef];
  }

  // EDIT PERSON
  // CATCH PEOPLE AND FEED FORM
  editPeople(dbSource,peopleId){
    this.dbS[dbSource].forEach(element => {
      if (element.id == peopleId){
       this.peopleForm = this.fb.group(element);
      }
    });
  }

  // DELETE PERSON
  deletePeople(dbSource,peopleId){
    ( async() => {
      this.crudCon.deletePeople(dbSource,peopleId);
      await delay(500);
      this.dbS[dbSource] = this.crudCon.dbS[dbSource];
      this.createForm();
    })();
  }

  // FEED FORM WITH "BLANK" VALUES
  createForm(){
    this.peopleForm = this.fb.group({
      nome : [''],
      nascimento: [''],
      email : [''],
      cpf: [''],
      endereco: [''],
      telefone: [''],
      obs: ['']
    });
  }

}
