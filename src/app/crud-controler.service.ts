import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Pessoa } from "./pessoa";

@Injectable({
  providedIn: 'root'
})

export class CrudControlerService {

  constructor(private httpConn:HttpClient) {
   }
  //apiEndpoint = "http://localhost:8080/"; 
  apiEndpoint = "http://localhost:4200/api/";
  // apiEndpoint = "http://192.168.0.213:5000"; // USED FOR TEST
  /*
  CONFIGURE PROXY TO FIX AND SUPPRESS CORS MESSAGE:
  https://medium.com/@gigioSouza/resolvendo-o-problema-do-cors-com-angular-2-e-o-angular-cli-7f7cb7aab3c2
  ADD VERBOSE TO START COMMAND: "ng serve --proxy-config proxy.config.js"
  */

  // INITIALIZE GLOBAL DB VALUES, USED TO PROVIDES THE SAME DATA FOR ALL COMPONENTS
  dbS = [[],[]];

   // CRUD METHODS
   // RETURN ALL PEOPLE
   getAllPeople(dbSource){
    this.dbS[dbSource] = []; //CLEAR DB ARRAY
    return this.httpConn.get(this.apiEndpoint)
      .toPromise()
      .then(response => {
        JSON.parse(JSON.stringify(response)).forEach(element => {
          this.dbS[dbSource].push(this.responseConvertToPeople(element));
        });
        console.log(this.dbS[dbSource]);
      })
  }

  // RETURN ESPECIFIED PEOPLE
  // NOT USED, BUT IMPLEMENTED
  getPeople(peopleId){
    return this.httpConn.get(this.apiEndpoint+peopleId)
      .toPromise()
      .then( response =>{
        console.log(JSON.parse(JSON.stringify(response)));
      })
    alert("Call getPeople");
  }

  // INSERT NEW VALUE, CALLING THE API AT ROOT PATH, AND SENDING PEOPLE OBJECT
  insertPeople(dbSource,people){
      console.log("insertPeople(): ")
      this.dbS[dbSource] = [];
      this.httpConn.post(this.apiEndpoint,people)
        .subscribe( response =>{
          // USED .forEach FOR FEEDING dbS[] ARRAY, TO TRANSFORM REQUEST AN PEOPLE OBJECT
          JSON.parse(JSON.stringify(response)).forEach(element => { 
            this.dbS[dbSource].push(this.responseConvertToPeople(element));
          });
        });
  }


  // MAKE A MERGE USING ROOT PATH TO, USING PUT METHOD
  editPeople(dbSource,people){
      this.httpConn.put(this.apiEndpoint,people)
        .subscribe( response =>{
          // THE SIMPLE WAY TO UPDATE array DATA
          this.dbS[dbSource] = JSON.parse(JSON.stringify(response));
        });
  }

  // DELETE REQUEST - ACCESS {id} PATH TO SUCCESS
  deletePeople(dbSource,peopleId){
    this.dbS[dbSource] = [];
    this.httpConn.delete(this.apiEndpoint+peopleId)
      .subscribe( response =>{
        this.dbS[dbSource] = JSON.parse(JSON.stringify(response));
      });
  }

  private addPeopleForTest(cond, db, data){
    let people = {"id":"1",
            "nome":"Carlos",
            "email":"teste@teste.com",
            "nascimento":"30/09/1987",
            "cpf":"0001"};
    cond ? this.dbS[db].push(data): '';
  }

  // GET SERVER RESPONSE AND CONVERT FOR THE PROPLE OBJECT
  // IMPORTANT TO FORMAT RESPONSE WHEN WE WORKING WITH OBJECTS
  private responseConvertToPeople(rawResponse):Pessoa{
    let people = new Pessoa(
      rawResponse.id,
      rawResponse.nome,
      rawResponse.nascimento,
      rawResponse.email,
      rawResponse.cpf,
      rawResponse.endereco,
      rawResponse.telefone,
      rawResponse.obs,
    )
    return people;
  }

}