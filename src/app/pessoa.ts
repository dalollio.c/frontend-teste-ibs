export class Pessoa {
    constructor(id: number, nome: string, nascimento: Date, email: string, cpf: string, 
        endereco: string, telefone: string, obs: string){
            this.id = id;
            this.nome = nome;
            this.nascimento = nascimento;
            this.email = email;
            this.cpf = cpf;
            this.endereco = endereco;
            this.telefone = telefone;
            this.obs = obs;

    }

    id: number;
    nome: string;
    nascimento: Date;
    email: string;
    cpf: string;
    endereco: string;
    telefone: string;
    obs: string;
}